<?php

namespace Drupal\sms_broadcast\Services;

class Sms {

    public function _prepare(&$package) {

        //get the username and password
        $config = \Drupal::config('sms_broadcast.settings');
        $username = $config->get('username');
        $password = $config->get('password');
        $target_url = $config->get('url');
        
        if(empty($username) || empty($password)) {
            throw new \exception("You must add your SMS Broadcast username and password into the admin configuration settings form.");
        }

        $package['username'] = $username;
        $package['password'] = $password;
    }

    public function send($package) {

        $this->_prepare($package);
        $required = array('username', 'password', 'to', 'from', 'message');
        $payload = [];
        foreach($required as $field) {
            if(!isset($package[$field]) || empty($package[$field])) {
                throw new \exception($package[$field] ." is required.");
            }
            $payload[] = "$field=" . rawurldecode($package[$field]);
        }

        $payload = implode("&", $payload);
        $config = \Drupal::config('sms_broadcast.settings');
        $url = $config->get('url');
        if(empty($url)) {
            throw new \exception("You must enter the SMS Broadcast URL in the admin configuration.");
        }

        try {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec ($ch);
            curl_close ($ch);
            return $output;    
        } catch ( \exception $e) {
            throw $e;
        }
    }

}