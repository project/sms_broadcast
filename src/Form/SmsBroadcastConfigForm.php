<?php

/**
 * @file
 * Contains \Drupal\sms_broadcast\Form\SmsBroadcastConfigForm.
 */

namespace Drupal\sms_broadcast\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SmsBroadcastConfigForm extends ConfigFormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'sms_broadcast_config_form';
    }

    /**
     * {@inheritdoc}
     */
    function getEditableConfigNames() {
        return [
          'sms_broadcast.settings',
        ];
      }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildForm($form, $form_state);
        $config = $this->config('sms_broadcast.settings');

        $form['username'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Username'),
            '#default_value' => $config->get('sms_broadcast.username'),
            '#required' => TRUE,
        );

        $form['password'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Password'),
            '#default_value' => $config->get('sms_broadcast.password'),
            '#required' => TRUE,
        );

        $form['url'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('SMS Broadcast URL'),
            '#default_value' => 'https://api.smsbroadcast.com.au/api-adv.php',
            '#required' => TRUE,
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $this->configFactory->getEditable('sms_broadcast.settings')
        ->set('username', $form_state->getValue('username'))
        ->set('password', $form_state->getValue('password'))
        ->set('url', $form_state->getValue('url'))
        ->save();

        parent::submitForm($form, $form_state);
    }
}