
INTRODUCTION
-----------------
This module was created to allow a website to send SMS message through SMS Broadcast (https://www.smsbroadcast.com.au/)
This module provide a service that can be use by other modules.

CONFIGURATION
-----------------
Install the module as usual 
Configure the module in "admin/config/sms-broadcast/settings"

EXAMPLE
-----------------
$sms = \Drupal::service('sms_broadcast.sms'); 

$to_phone_number = "123456789";
$from = "Company name";
$message = "Test SMS message";

$msg = [];
$msg['to'] = $to_phone_number;
$msg['from'] = $from;
$msg['message'] = $message;

try {
    $sms->send($msg);
} catch Exception($e) {
    // Log to watchdog $e->getMessage()
}

MAINTAINERS
-----------------
 * Finau H Kaufusi (finaukaufusi) - https://www.drupal.org/u/finaukaufusi